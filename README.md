#Citywire coding challenge
A small application to demonstrate refactoring and automated testing practices for Citywire.

## Build requirements

  - .NET 4.0
  - Visual Studio 2013
  - MSBuild
  
## Development dependancy requirements

  - NUnit
  - Moq
  
## Usage

Running the `.\build.ps1` command will:

  - Install dependancies
  - Build application
  - Run all unit tests
  - Output test results to `.\src\TestResults`
