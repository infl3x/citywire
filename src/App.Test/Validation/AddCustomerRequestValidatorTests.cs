﻿using App.Model;
using App.Validation;
using FluentValidation.Results;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Test.Validation
{
    [TestFixture]
    public class AddCustomerRequestValidatorTests
    {

        private AddCustomerRequestValidator _validator;

        [SetUp]
        public void SetUp()
        {
            _validator = new AddCustomerRequestValidator();
        }

        [Test]
        public void ShouldAllowValidAddCustomerRequest()
        {
            ValidationResult validationResult = _validator.Validate(new AddCustomerRequest()
            {
                FirstName = "This is valid",
                Surname = "So is this",
                Email = "someone@somewhere.com",
                DateOfBirth = DateTime.Today.AddYears(21)
            });

            Assert.That(validationResult.IsValid, Is.True);
        }

        #region First Name

        [Test]
        public void ShouldNotAllowNullFirstName()
        {
            ValidationResult validationResult = _validator.Validate(new AddCustomerRequest()
            {
                FirstName = null,
            });

            Assert.That(validationResult.IsValid, Is.False);
            Assert.That(validationResult.Errors.Any(e => e.PropertyName == "FirstName"));
        }

        [Test]
        public void ShouldNotAllowEmptyFirstName()
        {
            ValidationResult validationResult = _validator.Validate(new AddCustomerRequest()
            {
                FirstName = String.Empty,
            });

            Assert.That(validationResult.IsValid, Is.False);
            Assert.That(validationResult.Errors.Any(e => e.PropertyName == "FirstName"));
        }

        #endregion

        #region Surname

        [Test]
        public void ShouldNotAllowNullSurname()
        {
            ValidationResult validationResult = _validator.Validate(new AddCustomerRequest()
            {
                Surname = null,
            });

            Assert.That(validationResult.IsValid, Is.False);
            Assert.That(validationResult.Errors.Any(e => e.PropertyName == "Surname"));
        }

        [Test]
        public void ShouldNotAllowEmptySurname()
        {
            ValidationResult validationResult = _validator.Validate(new AddCustomerRequest()
            {
                Surname = String.Empty,
            });

            Assert.That(validationResult.IsValid, Is.False);
            Assert.That(validationResult.Errors.Any(e => e.PropertyName == "Surname"));
        }

        #endregion

        #region Email

        [Test]
        public void ShouldNotAllowEmailWithoutAtSymbol()
        {
            ValidationResult validationResult = _validator.Validate(new AddCustomerRequest()
            {
                Email = ".",
            });

            Assert.That(validationResult.IsValid, Is.False);
            Assert.That(validationResult.Errors.Any(e => e.PropertyName == "Email"));
        }

        [Test]
        public void ShouldNotAllowEmailWithDotSymbol()
        {
            ValidationResult validationResult = _validator.Validate(new AddCustomerRequest()
            {
                Email = "@",
            });

            Assert.That(validationResult.IsValid, Is.False);
            Assert.That(validationResult.Errors.Any(e => e.PropertyName == "Email"));
        }

        #endregion

        #region Age

        [Test]
        public void ShouldNotAllowAgeLessThan22()
        {
            ValidationResult validationResult = _validator.Validate(new AddCustomerRequest()
            {
                DateOfBirth = DateTime.Today.AddYears(21).AddSeconds(-1),
            });

            Assert.That(validationResult.IsValid, Is.False);
            Assert.That(validationResult.Errors.Any(e => e.PropertyName == "DateOfBirth"));
        }

        #endregion

    }
}
