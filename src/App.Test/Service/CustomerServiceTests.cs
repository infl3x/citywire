﻿using App.DataAccess.Repository;
using App.Model;
using App.Service;
using App.Validation;
using FluentValidation;
using FluentValidation.Results;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Test.Service
{
    [TestFixture]
    public class CustomerServiceTests
    {
        private Mock<ICompanyRepository> _companyRepository;
        private Mock<ICustomerRepository> _customerRepository;
        private Mock<IAddCustomerRequestValidator> _addCustomerRequestValidator;
        private Mock<ICreditLimitService> _creditLimitService;

        private CustomerService _customerService;

        [SetUp]
        public void SetUp()
        {
            _companyRepository = new Mock<ICompanyRepository>();
            _customerRepository = new Mock<ICustomerRepository>();
            _addCustomerRequestValidator = new Mock<IAddCustomerRequestValidator>();
            _creditLimitService = new Mock<ICreditLimitService>();

            _customerService = new CustomerService(_companyRepository.Object, _customerRepository.Object, _addCustomerRequestValidator.Object, _creditLimitService.Object);
        }

        [Test]
        public void ShouldReturnFalseWhenValidationFails()
        {
            _addCustomerRequestValidator.Setup(v => v.Validate(It.IsAny<AddCustomerRequest>())).Returns(new ValidationResult(new List<ValidationFailure>() { new ValidationFailure("FirstName", "Error") }));

            Assert.That(_customerService.AddCustomer(new AddCustomerRequest()), Is.False);
        }

        [Test]
        public void ShouldSetCreditLimitOnCustomer()
        {
            const string Email = "someone@somewhere.com";
            const string FirstName = "first";
            const string Surname = "surname";
            const string CompanyName = "company";

            AddCustomerRequest addCustomerRequest = new AddCustomerRequest()
            {
                CompanyId = 123,
                DateOfBirth = DateTime.Today.AddYears(-100),
                Email = Email,
                FirstName = FirstName,
                Surname = Surname
            };

            _companyRepository.Setup(c => c.GetById(123)).Returns(new Company() { Name = CompanyName });
            _addCustomerRequestValidator.Setup(v => v.Validate(It.IsAny<AddCustomerRequest>())).Returns(new ValidationResult());

            _customerService.AddCustomer(addCustomerRequest);

            _creditLimitService.Verify(s => s.SetCreditLimit(It.Is<Customer>(c =>
                c.Company.Name == CompanyName &&
                c.DateOfBirth == DateTime.Today.AddYears(-100) &&
                c.EmailAddress == Email &&
                c.FirstName == FirstName && 
                c.Surname == Surname
            )), Times.Once());
        }

        [Test]
        public void ShouldNotAddCustomerWithLowCreditLimit()
        {
            _addCustomerRequestValidator.Setup(v => v.Validate(It.IsAny<AddCustomerRequest>())).Returns(new ValidationResult());

            _creditLimitService.Setup(s => s.SetCreditLimit(It.IsAny<Customer>())).Callback<Customer>(c => {
                c.HasCreditLimit = true;
                c.CreditLimit = 499;
            });

            Assert.That(_customerService.AddCustomer(new AddCustomerRequest()), Is.False);
        }

        [Test]
        public void ShouldAddCustomerWithNoCreditLimit()
        {
            _addCustomerRequestValidator.Setup(v => v.Validate(It.IsAny<AddCustomerRequest>())).Returns(new ValidationResult());

            _creditLimitService.Setup(s => s.SetCreditLimit(It.IsAny<Customer>())).Callback<Customer>(c =>
            {
                c.HasCreditLimit = false;
                c.CreditLimit = 0;
            });

            Assert.That(_customerService.AddCustomer(new AddCustomerRequest()), Is.True);
        }

        [Test]
        public void ShouldAddCustomerWithHighCreditLimit()
        {
            _addCustomerRequestValidator.Setup(v => v.Validate(It.IsAny<AddCustomerRequest>())).Returns(new ValidationResult());

            _creditLimitService.Setup(s => s.SetCreditLimit(It.IsAny<Customer>())).Callback<Customer>(c =>
            {
                c.HasCreditLimit = false;
                c.CreditLimit = 500;
            });

            Assert.That(_customerService.AddCustomer(new AddCustomerRequest()), Is.True);
        }

    }
}
