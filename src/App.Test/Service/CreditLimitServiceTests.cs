﻿using App.Model;
using App.Proxy;
using App.Service;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Test.Service
{
    [TestFixture]
    public class CreditLimitServiceTests
    {
        private Mock<ICustomerCreditProxy> _customerCreditProxy;
        private CreditLimitService _creditLimitService;

        [SetUp]
        public void SetUp()
        {
            _customerCreditProxy = new Mock<ICustomerCreditProxy>();
            _creditLimitService = new CreditLimitService(_customerCreditProxy.Object);
        }

        [Test]
        public void ShouldApplyCreditLimit()
        {
            const int CreditLimit = 99;
            _customerCreditProxy.Setup(c => c.GetCreditLimit(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(CreditLimit);

            Customer customer = new Customer()
            {
                Company = new Company()
            };
            _creditLimitService.SetCreditLimit(customer);

            Assert.That(customer.HasCreditLimit, Is.True);
            Assert.That(customer.CreditLimit, Is.EqualTo(CreditLimit));
        }

        [Test]
        public void ShouldNotApplyCreditLimitWhenCustomerIsVeryImportantClient()
        {
            Company company = new Company()
            {
                Name = "VeryImportantClient"
            };
            Customer customer = new Customer()
            {
                Company = company
            };
            _creditLimitService.SetCreditLimit(customer);

            Assert.That(customer.HasCreditLimit, Is.False);
            Assert.That(customer.CreditLimit, Is.EqualTo(0));
        }

        [Test]
        public void ShouldDoubleCreditLimitWhenCustomerIsImportantClient()
        {
            const int CreditLimit = 5;
            _customerCreditProxy.Setup(c => c.GetCreditLimit(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(CreditLimit);

            Company company = new Company()
            {
                Name = "ImportantClient"
            };
            Customer customer = new Customer()
            {
                Company = company
            };
            _creditLimitService.SetCreditLimit(customer);

            Assert.That(customer.HasCreditLimit, Is.True);
            Assert.That(customer.CreditLimit, Is.EqualTo(CreditLimit * 2));
        }

    }
}
