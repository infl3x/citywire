﻿using App.DataAccess;
using App.DataAccess.Repository;
using App.Proxy;
using App.Service;
using App.Validation;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Infrastructure
{
    public class AppModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAddCustomerRequestValidator>().To<AddCustomerRequestValidator>();
            Bind<ICustomerCreditProxy>().To<CustomerCreditProxy>();
            Bind<ICustomerService>().To<CustomerService>();
            Bind<ICompanyRepository>().To<CompanyRepository>();
            Bind<ICustomerRepository>().To<CustomerRepository>();
            Bind<ICreditLimitService>().To<CreditLimitService>();
        }
    }
}
