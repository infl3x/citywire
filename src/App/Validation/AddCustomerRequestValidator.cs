﻿using App.Model;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Validation
{
    public interface IAddCustomerRequestValidator : IValidator<AddCustomerRequest>
    {
    }

    public class AddCustomerRequestValidator : AbstractValidator<AddCustomerRequest>, IAddCustomerRequestValidator
    {
        public AddCustomerRequestValidator()
        {
            RuleFor(c => c.FirstName).NotNull().NotEmpty();
            RuleFor(c => c.Surname).NotNull().NotEmpty();
            RuleFor(c => c.Email).Must(IsValidEmail).When(c => !String.IsNullOrEmpty(c.Email));
            RuleFor(c => c.DateOfBirth).GreaterThanOrEqualTo(DateTime.Today.AddYears(21));
        }

        private bool IsValidEmail(string email)
        {
            return email.Contains("@") && email.Contains(".");
        }
    }
}
