﻿using App.Model;
using App.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Service
{
    public interface ICreditLimitService
    {
        void SetCreditLimit(Customer customer);
    }

    public class CreditLimitService : ICreditLimitService
    {
        private const string VeryImportantClientName = "VeryImportantClient";
        private const string ImportantClientName = "ImportantClient";

        private readonly ICustomerCreditProxy _customerCreditProxy;

        public CreditLimitService(ICustomerCreditProxy customerCreditProxy)
        {
            _customerCreditProxy = customerCreditProxy;
        }

        public void SetCreditLimit(Customer customer)
        {
            if (customer.Company.Name == VeryImportantClientName)
            {
                // Skip credit check
                customer.HasCreditLimit = false;
            }
            else if (customer.Company.Name == ImportantClientName)
            {
                // Do credit check and double credit limit
                customer.HasCreditLimit = true;

                int creditLimit = _customerCreditProxy.GetCreditLimit(customer.FirstName, customer.Surname, customer.DateOfBirth);
                customer.CreditLimit = creditLimit * 2;
            }
            else
            {
                // Do credit check
                customer.HasCreditLimit = true;

                int creditLimit = _customerCreditProxy.GetCreditLimit(customer.FirstName, customer.Surname, customer.DateOfBirth);
                customer.CreditLimit = creditLimit;
            }
        }
    }
}
