﻿using App.DataAccess.Repository;
using App.Model;
using App.Proxy;
using App.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Service
{
    public interface ICustomerService
    {
        bool AddCustomer(AddCustomerRequest addCustomerRequest);
    }

    public class CustomerService : ICustomerService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IAddCustomerRequestValidator _addCustomerRequestValidator;
        private readonly ICreditLimitService _creditLimitService;

        public CustomerService(ICompanyRepository companyRepository, ICustomerRepository customerRepository, IAddCustomerRequestValidator addCustomerRequestValidator, ICreditLimitService creditLimitService)
        {
            _companyRepository = companyRepository;
            _customerRepository = customerRepository;
            _addCustomerRequestValidator = addCustomerRequestValidator;
            _creditLimitService = creditLimitService;
        }

        public bool AddCustomer(AddCustomerRequest addCustomerRequest)
        {
            if (!_addCustomerRequestValidator.Validate(addCustomerRequest).IsValid)
                return false;

            var company = _companyRepository.GetById(addCustomerRequest.CompanyId);

            var customer = new Customer
            {
                Company = company,
                DateOfBirth = addCustomerRequest.DateOfBirth,
                EmailAddress = addCustomerRequest.Email,
                FirstName = addCustomerRequest.FirstName,
                Surname = addCustomerRequest.Surname
            };

            _creditLimitService.SetCreditLimit(customer);

            if (customer.HasCreditLimit && customer.CreditLimit < 500)
            {
                return false;
            }

            _customerRepository.AddCustomer(customer);

            return true;
        }
    }
}
