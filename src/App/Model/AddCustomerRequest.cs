﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Model
{
    public class AddCustomerRequest
    {
        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public DateTime DateOfBirth { get; set; }

        public int CompanyId { get; set; }
    }
}
