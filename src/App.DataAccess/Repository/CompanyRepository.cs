﻿using App.DataAccess.Data;
using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.DataAccess.Repository
{
    public interface ICompanyRepository
    {
        Company GetById(int id);
    }

    public class CompanyRepository : ICompanyRepository
    {
        public Company GetById(int id)
        {
            return CompanyDataAccess.GetById(id);
        }
    }
}
