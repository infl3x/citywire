Write-Host ""
Write-Host "Downloading NuGet packages..." -ForegroundColor "Cyan"

$nugetExe = Resolve-Path ".\bld\NuGet.exe"

& $nugetExe i ".\bld\packages.config" -o ".\src\packages"
& $nugetExe i ".\src\App\packages.config" -o ".\src\packages"
& $nugetExe i ".\src\App.Test\packages.config" -o ".\src\packages"

Write-Host "Successfully downloaded NuGet packages!" -ForegroundColor "DarkGreen"
