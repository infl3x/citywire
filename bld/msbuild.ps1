# valid versions are [2.0, 3.5, 4.0]

Write-Host ""
Write-Host "Looking for MSBuild for .NET 4.x..." -ForegroundColor "Cyan"

$dotNetVersion = "4.0"
$registryKey = "HKLM:\software\Microsoft\MSBuild\ToolsVersions\$dotNetVersion"
$registryProperty = "MSBuildToolsPath"

$msbuildExe = Join-Path -path (Get-ItemProperty $registryKey).$registryProperty -childpath "msbuild.exe"

if(!(Test-Path $msbuildExe)) {
  Write-Error "Unable to locate MSBuild for .NET 4.x!"
}

Write-Host "Using MSBuild path $($msbuildExe)"

Write-Host "Cleaning Debug solution..." -ForegroundColor "Cyan"
& $msbuildExe ".\src\App.sln" /t:Clean "/p:Configuration=Debug" /verbosity:minimal /nologo
if($LastExitCode -ne 0) {
	Write-Error "Cleaning errors detected!"
}
Write-Host "Debug solution successfully cleaned!" -ForegroundColor "DarkGreen"

Write-Host "Cleaning Release solution..." -ForegroundColor "Cyan"
& $msbuildExe ".\src\App.sln" /t:Clean "/p:Configuration=Release" /verbosity:minimal /nologo
if($LastExitCode -ne 0) {
	Write-Error "Cleaning errors detected!"
}
Write-Host "Release solution successfully cleaned!" -ForegroundColor "DarkGreen"

Write-Host "Building solution..." -ForegroundColor "Cyan"
& $msbuildExe ".\src\App.sln" /t:Build "/p:Configuration=Release" /verbosity:minimal /nologo
if($LastExitCode -ne 0) {
	Write-Error "Build errors detected!"
}
Write-Host "Solution successfully built!" -ForegroundColor "DarkGreen"

